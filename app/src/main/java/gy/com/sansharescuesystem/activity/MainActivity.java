package gy.com.sansharescuesystem.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.baidu.mapapi.SDKInitializer;

import gy.com.sansharescuesystem.R;
import gy.com.sansharescuesystem.base.BaseActivity;
import gy.com.sansharescuesystem.fragment.CountFragment;
import gy.com.sansharescuesystem.fragment.HistoryFragment;
import gy.com.sansharescuesystem.fragment.RescueFragment;
import gy.com.sansharescuesystem.utils.Constant;
import gy.com.sansharescuesystem.utils.UIUtils;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    private ImageButton mBtnRescue;
    private ImageButton mBtnHistory;
    private ImageButton mBtnCount;
    private FragmentManager mManager;
    private RescueFragment mRescueFragment;
    private HistoryFragment mHistoryFragment;
    private CountFragment mCountFragment;
    private Fragment currentFragment;
    //用户
    private ImageButton userImgBtn;
    //救援实时监测次数
    private TextView monitoringTimesImgBtn;
    private TextView titleTw;
    //广播
    private MyBroadCastReceiver myBroadCastReceiver = new MyBroadCastReceiver();
    private static final String TOTAL_SOS_MESSAGE = "total_sos_message";
    //flag
    private static boolean is_message_sended = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        SDKInitializer.initialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        initView();
        setWidgetListener();
        showFragment(mRescueFragment);
        IntentFilter filter = new IntentFilter();
        filter.addAction(TOTAL_SOS_MESSAGE);
        registerReceiver(myBroadCastReceiver,filter);
    }

    @Override
    protected void initView() {
        mBtnRescue = (ImageButton) findViewById(R.id.btn_rescue);
        mBtnHistory = (ImageButton) findViewById(R.id.btn_history);
        mBtnCount = (ImageButton) findViewById(R.id.btn_count);
        userImgBtn = (ImageButton) findViewById(R.id.btn_user);
        monitoringTimesImgBtn=(TextView) findViewById(R.id.btn_monitoring_times);
        titleTw= (TextView) findViewById(R.id.tv_title);
        mManager = this.getSupportFragmentManager();

        mRescueFragment = new RescueFragment();
        mHistoryFragment = new HistoryFragment();
        mCountFragment = new CountFragment();
        currentFragment = mRescueFragment;
    }

    @Override
    protected void setWidgetListener() {
        mBtnHistory.setOnClickListener(this);
        mBtnRescue.setOnClickListener(this);
        mBtnCount.setOnClickListener(this);
        userImgBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_rescue:
                mBtnCount.setImageResource(R.drawable.data);
                mBtnHistory.setImageResource(R.drawable.history);
                userImgBtn.setVisibility(View.VISIBLE);
                monitoringTimesImgBtn.setVisibility(View.VISIBLE);
                titleTw.setText("救援实时监测");
                showFragment(mRescueFragment);
                //避免重复点击救援按钮造成oom
                if(!is_message_sended){
                    Intent intent2  = new Intent(Constant.TO_RESCUE_PAGE);
                    intent2.putExtra("flag",Constant.FLAG_RESCUE);
                    sendBroadcast(intent2);
                }
                is_message_sended=true;
                break;
            case R.id.btn_history:
                is_message_sended=false;
                mBtnCount.setImageResource(R.drawable.data);
                mBtnHistory.setImageResource(R.drawable.history_light);
                userImgBtn.setVisibility(View.GONE);
                monitoringTimesImgBtn.setVisibility(View.GONE);
                titleTw.setText("历史救援");
                showFragment(mHistoryFragment);
                Intent intent1 = new Intent(Constant.TO_HISTORY_PAGE);
                intent1.putExtra("flag",Constant.FLAG_HISTORY);
                sendBroadcast(intent1);
                break;
            case R.id.btn_count:
                is_message_sended=false;
                mBtnCount.setImageResource(R.drawable.data_light);
                mBtnHistory.setImageResource(R.drawable.history);
                userImgBtn.setVisibility(View.GONE);
                monitoringTimesImgBtn.setVisibility(View.GONE);
                titleTw.setText("数据统计");
                showFragment(mCountFragment);
                Intent intent3 = new Intent(Constant.TO_COUNT_PAGE);
                intent3.putExtra("flag",Constant.FLAG_COUNT);
                sendBroadcast(intent3);
                break;
            case R.id.btn_user:
                startActivity(new Intent(this, ModifyPwdActivity.class));
                break;
        }
    }

    public void showFragment(Fragment fragment) {
        FragmentManager manager = this.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (!fragment.isAdded()) {
            transaction.add(R.id.fl_fragment_container, fragment);
        }
        transaction.hide(currentFragment);
        transaction.show(fragment);
        transaction.commit();
        currentFragment = fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();


    }

    private long exitTime = 0;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN){
            if((System.currentTimeMillis()-exitTime) > 2000){
                UIUtils.showToast(getApplicationContext(), "再按一次退出程序");
                exitTime = System.currentTimeMillis();
            } else {
                finish();
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    class MyBroadCastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()){
                case TOTAL_SOS_MESSAGE:
                    String total = intent.getStringExtra("total");
                    monitoringTimesImgBtn.setText(total);
                    break;
            }
        }
    }
}
