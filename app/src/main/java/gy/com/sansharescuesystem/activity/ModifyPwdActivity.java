package gy.com.sansharescuesystem.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.HashMap;
import java.util.Map;

import gy.com.sansharescuesystem.R;
import gy.com.sansharescuesystem.base.BaseActivity;
import gy.com.sansharescuesystem.callback.LoginCallBack;
import gy.com.sansharescuesystem.entity.LoginBean;
import gy.com.sansharescuesystem.modle.impl.ModifyPwdModelImpl;
import gy.com.sansharescuesystem.utils.NetWorkUtils;
import gy.com.sansharescuesystem.utils.SharePreferenceUtils;
import gy.com.sansharescuesystem.utils.UIUtils;

/**
 * function:修改密码
 * author:zsf
 * time:2017.07.15
 */
public class ModifyPwdActivity extends BaseActivity implements View.OnClickListener{

    private EditText oldPwdEdt;
    private EditText newPwdEdt;
    private EditText newPwdConfirmEdt;
    private Button submitBtn;
    private ImageView backImgView;
    private static final int ACCESS_SUCCESS = 0;
    private static final int ACCESS_ERROR = 1;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case ACCESS_SUCCESS:
                    LoginBean loginBean = (LoginBean) msg.obj;
                    if (loginBean.getFlag().equals("1") ) {
                        Intent intent=new Intent(ModifyPwdActivity.this, LoginActivity.class);
                        intent.putExtra("modify","success");
                        startActivity(intent);
                        finish();
                    } else if(loginBean.getFlag().equals("0")){
                        UIUtils.showToast(getApplicationContext(), "旧密码错误！");
                    }
                    break;

                case ACCESS_ERROR:
                    UIUtils.showToast(getApplicationContext(), "请求数据异常，请稍后重试！");
                    break;

                default:

                    break;


            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_pwd);
        initView();
        setWidgetListener();
    }

    @Override
    protected void initView() {
        backImgView= (ImageView) findViewById(R.id.backImgVw);
        oldPwdEdt = (EditText) findViewById(R.id.oldPwdEdt);
        newPwdEdt = (EditText) findViewById(R.id.newPwdEdt);
        newPwdConfirmEdt = (EditText) findViewById(R.id.newPwdConfirmEdt);
        submitBtn= (Button) findViewById(R.id.submitBtn);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void setWidgetListener() {
        submitBtn.setOnClickListener(this);
        backImgView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
    switch (v.getId()){
        case R.id.backImgVw:
            finish();
            break;
        case R.id.submitBtn:
            if(oldPwdEdt.getText().toString().equals("")){
                UIUtils.showToast(getApplicationContext(), "原密码不能为空！");
                return;
            }
            if(newPwdEdt.getText().toString().equals("")){
                UIUtils.showToast(getApplicationContext(), "新密码不能为空！");
                return;
    }

            if(newPwdConfirmEdt.getText().toString().equals("")){
                UIUtils.showToast(getApplicationContext(), "确认密码不能为空！");
                return;
            }
            if(!newPwdEdt.getText().toString().equals(newPwdConfirmEdt.getText().toString())){
                UIUtils.showToast(getApplicationContext(), "新密码和确认新密码不一致！");
                return;
            }

            if (NetWorkUtils.IsHaveInternet(this)) {
                submit();
            } else {
                UIUtils.showToast(this, "请确认是否有网络！");
            }
            break;

        default:
            break;
    }
    }

private void submit(){

    Map<String, String> map = new HashMap<String, String>();
    map.put("username",SharePreferenceUtils.getStringPerferences(this,"username",""));
    map.put("oldpass", oldPwdEdt.getText().toString());
    map.put("newpass", newPwdEdt.getText().toString());
    map.put("newpass2", newPwdConfirmEdt.getText().toString());
    ModifyPwdModelImpl modifyPwdModel = new ModifyPwdModelImpl();
    modifyPwdModel.getData(new LoginCallBack() {
        @Override
        public void onSuccse(LoginBean loginBean) {
            Message message = handler.obtainMessage();
            message.what = ACCESS_SUCCESS;
            message.obj = loginBean;
            handler.sendMessage(message);


        }

        @Override
        public void onFailed() {
            Message message = handler.obtainMessage();
            message.what = ACCESS_ERROR;
            handler.sendMessage(message);
        }
    }, map);



}

}
