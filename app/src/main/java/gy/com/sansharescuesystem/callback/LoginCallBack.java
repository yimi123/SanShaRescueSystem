package gy.com.sansharescuesystem.callback;

import gy.com.sansharescuesystem.entity.LoginBean;

/**
 * Created by zsf on 2017-07-18.
 * function:回调
 */
public interface LoginCallBack {
    void onSuccse(LoginBean loginBean);
    void onFailed();
}
