package gy.com.sansharescuesystem.callback;

import gy.com.sansharescuesystem.base.BaseFragment;

/**
*对子fragment的callback操作
*@author huangbo
*create at 2016/11/15 17:57
*/
public interface SelectSonFragmentCallBack  {
    void selectFragment(BaseFragment pBaseFragment);//得到子fragment
    void goneBottomLayout();//隐藏底部bar
    void visibleBottomLayout();//显示底部bar
}
