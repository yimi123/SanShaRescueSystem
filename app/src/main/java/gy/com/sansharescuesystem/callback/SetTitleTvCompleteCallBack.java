package gy.com.sansharescuesystem.callback;


/**
*监听标题完成按钮操作
*@author huangbo
*create at 2016/11/14 13:27
*/
public interface SetTitleTvCompleteCallBack {
    void toCompleteEditHead();
}
