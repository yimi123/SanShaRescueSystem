package gy.com.sansharescuesystem.callback;


public interface AcyncCallBack {
    void onSuccess(Object obj);
    void onFailed();
}
