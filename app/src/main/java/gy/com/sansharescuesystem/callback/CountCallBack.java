package gy.com.sansharescuesystem.callback;

import gy.com.sansharescuesystem.entity.CountBean;
import gy.com.sansharescuesystem.entity.LoginBean;

/**
 * Created by zsf on 2017-07-18.
 * function:回调
 */
public interface CountCallBack {
    void onSuccess(Object obj);
    void onFailed();
}
