package gy.com.sansharescuesystem.callback;


/**
*修改标题
*@author huangbo
*create at 2016/11/14 13:27
*/
public interface SetTitleCallBack {
    void initTitle(String pTitle, int pTxtColor, int pBgColor, int pImgBack, int pImgEdit, int pTvComplete);
}
