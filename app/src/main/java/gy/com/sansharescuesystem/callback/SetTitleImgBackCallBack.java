package gy.com.sansharescuesystem.callback;


/**
*监听标题返回键操作
*@author huangbo
*create at 2016/11/14 13:27
*/
public interface SetTitleImgBackCallBack {
    void toBack();
}
