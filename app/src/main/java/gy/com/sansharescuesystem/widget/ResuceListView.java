package gy.com.sansharescuesystem.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by 陈秋霖 on 2017-07-17.
 */

public class ResuceListView extends ListView{
    public ResuceListView(Context context) {
        super(context);
    }

    public ResuceListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ResuceListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public void setListScrolable(){

    }

    @Override
    public void onTouchModeChanged(boolean isInTouchMode) {
        super.onTouchModeChanged(isInTouchMode);
    }
}
