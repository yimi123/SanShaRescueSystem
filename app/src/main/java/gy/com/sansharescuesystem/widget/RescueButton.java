package gy.com.sansharescuesystem.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import gy.com.sansharescuesystem.R;

/**
 * Created by 陈秋霖 on 2017-07-12.
 */

public class RescueButton extends Button{
    private static String TAG="RescueButton";
    //按钮背景色
    private int backColor =0;
    //按钮被按下时背景色
    private int backColorPress = 0;
    //按钮背景图片
    private Drawable backGroundDrawable = null;
    //按钮被按下时背景图片
    private Drawable backGroundDrwawablePress = null;
    //按钮文字颜色
    private ColorStateList textColor = null;
    //按钮被按下时文字颜色
    private ColorStateList textColorPress = null;
    //
    private GradientDrawable gradientDrawable = null;
    //是否设置圆角或者圆形等样式
    private boolean fillet = false;
    //标识onTouch的返回值 用来解决onClick和onTouch冲突的问题
    private boolean isCost = true;

    public RescueButton(Context context) {
        super(context);
    }

    public RescueButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RescueButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //拿到布局文件中设置的参数
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RescueButton,defStyleAttr,0);
        if(a!=null){
            //设置背景颜色
            ColorStateList colorList = a.getColorStateList(R.styleable.RescueButton_backColor);
            if(colorList!=null){
                backColor = colorList.getColorForState(getDrawableState(),0);
                if(backColor!=0){
                    setBackgroundColor(backColor);
                }
            }
            //设置按钮被按下时的背景颜色
            ColorStateList colorListPress = a.getColorStateList(R.styleable.RescueButton_backColorPress);
            if(colorListPress!=null){
                backColorPress = colorListPress.getColorForState(getDrawableState(),0);
            }
            //设置背景图片，若backColor和backGroundDrawable同时存在，backGroundDrawable有效
            backGroundDrawable = a.getDrawable(R.styleable.RescueButton_backGroundImage);
            if(backGroundDrawable!=null){
                setBackground(backGroundDrawable);
            }
            //保存按钮被按下时的图片，不设置
            backGroundDrwawablePress = a.getDrawable(R.styleable.RescueButton_backGroundImagePress);
            //文字颜色
            textColor = a.getColorStateList(R.styleable.RescueButton_textColor);
            if(textColor!=null){
                setTextColor(textColor);
            }
            //保存按钮被按下时候的字体颜色，不设置
            textColorPress = a.getColorStateList(R.styleable.RescueButton_textColorPress);
            //设置圆角或圆形样式的背景色
            fillet = a.getBoolean(R.styleable.RescueButton_fillet,false);
            if(fillet){
                getGradientDrawable();
                if(backColor!=0){
                    gradientDrawable.setColor(backColor);
                    setBackground(gradientDrawable);
                }
            }
            //设置圆角矩形的角度
            float radius = a.getFloat(R.styleable.RescueButton_radius,0);
            if(fillet&&radius!=0){
                setRadius(radius);
            }
            //设置按钮形状
            int shape = a.getInteger(R.styleable.RescueButton_shape,0);
            if(fillet&&shape!=0){
                setShape(shape);
            }
            a.recycle();
        }
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return setTouchStyle(event.getAction());
            }
        });
    }

    private boolean setTouchStyle(int action) {
        if(action == MotionEvent.ACTION_DOWN){
            if(backColorPress!=0){
                if(fillet){
                    gradientDrawable.setColor(backColorPress);
                    setBackground(gradientDrawable);
                }else{
                    setBackgroundColor(backColorPress);
                }
            }
            if(backGroundDrwawablePress!=null){
                setBackground(backGroundDrwawablePress);
            }
            if(textColorPress!=null){
                setTextColor(textColorPress);
            }
        }
        if(action==MotionEvent.ACTION_UP){
            if(backColor!=0){
                if(fillet){
                    gradientDrawable.setColor(backColor);
                    setBackground(gradientDrawable);
                }else{
                    setBackgroundColor(backColor);
                }
            }
            if(backGroundDrawable!=null){
                setBackground(backGroundDrawable);
            }
            if(textColor!=null){
                setTextColor(textColor);
            }
        }
        return isCost;
    }

    //重写setOnClickListener方法，解决onTouch和onClick冲突问题
    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        isCost=false;
    }
    //设置按钮的背景色
    public void setBackColor(int backColor){
        this.backColor = backColor;
        if(fillet){
            gradientDrawable.setColor(backColor);
            setBackground(gradientDrawable);
        }else{
            setBackgroundColor(backColor);
        }
    }
    //设置按下时的背景色
    public void setBackColorPress(int backColorPress){
        this.backColorPress = backColorPress;
    }
    //设置按钮的背景图片
    public void setBackGroundDrawable(Drawable backGroundDrawable){
        this.backGroundDrawable = backGroundDrawable;
        setBackground(backGroundDrawable);
    }
    //设置被按下时的背景图片
    public void setBackGroundDrwawablePress(){
        this.backGroundDrwawablePress = backGroundDrwawablePress;
    }
    //设置文字颜色
    public void setTextColor(int textColor){
        if(textColor==0)return;
        this.textColor = ColorStateList.valueOf(textColor);
        super.setTextColor(textColor);
    }
    //被按下时的字体颜色
    public void setTextColorPress(int textColorPress){
        if(textColorPress!=0){
            this.textColorPress = ColorStateList.valueOf(textColorPress);
        }
    }
    //设置按钮是否设置圆角或者圆形等样式
    public void setFillet(boolean fillet){
        this.fillet=fillet;
        getGradientDrawable();
    }
    //控制圆角角度
    public void setRadius(float radius) {
        if(!fillet) return;
        getGradientDrawable();
        gradientDrawable.setCornerRadius(radius);
        setBackground(gradientDrawable);
    }

    public void setShape(int shape) {
        if(!fillet) return;
        getGradientDrawable();
        gradientDrawable.setShape(shape);
        setBackground(gradientDrawable);
    }

    public void getGradientDrawable() {
        if(gradientDrawable==null){
            gradientDrawable = new GradientDrawable();
        }
    }
}
