package gy.com.sansharescuesystem.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SPUtil {

	public static final String PREFERENCE_NAME = "config";

	public static SharedPreferences getSharedPreferences(Context context) {
		return context == null ? null : context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
	}

	public static boolean putString(Context context, String key, String value) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(key, value);
		return editor.commit();
	}


	public static String getString(Context context, String key) {
		return getString(context, key, null);
	}

	public static String getString(Context context, String key,
								   String defaultValue) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
		return settings.getString(key, defaultValue);
	}


	public static boolean putInt(Context context, String key, int value) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(key, value);
		return editor.commit();
	}


	public static int getInt(Context context, String key) {
		return getInt(context, key, -1);
	}


	public static int getInt(Context context, String key, int defaultValue) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
		return settings.getInt(key, defaultValue);
	}

	public static boolean putLong(Context context, String key, long value) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(key, value);
		return editor.commit();
	}

	public static long getLong(Context context, String key) {
		return getLong(context, key, -1);
	}

	public static long getLong(Context context, String key, long defaultValue) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
		return settings.getLong(key, defaultValue);
	}

	public static boolean putFloat(Context context, String key, float value) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putFloat(key, value);
		return editor.commit();
	}

	public static float getFloat(Context context, String key) {
		return getFloat(context, key, -1);
	}

	public static float getFloat(Context context, String key, float defaultValue) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
		return settings.getFloat(key, defaultValue);
	}

	public static boolean putBoolean(Context context, String key, boolean value) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(key, value);
		return editor.commit();
	}


	public static boolean getBoolean(Context context, String key) {
		return getBoolean(context, key, false);
	}

	public static boolean getBoolean(Context context, String key,
									 boolean defaultValue) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFERENCE_NAME, Context.MODE_PRIVATE);
		return settings.getBoolean(key, defaultValue);
	}

	public static boolean saveBitmap(Context context, String key, Bitmap obj) {
		try {
			boolean flag = false;
			SharedPreferences.Editor sharedata = context.getSharedPreferences("shared_obj_", 0).edit();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			obj.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
			String imageString = new String(Base64.encode(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
			sharedata.putString(key, imageString);
			flag = sharedata.commit();
			byteArrayOutputStream.close();
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.error("json", "保存Bitmap失败");
			return false;
		}
	}


	public static synchronized Bitmap readBitmap(Context context, String key) {
		try {
			SharedPreferences sharedPreferences = context.getSharedPreferences("shared_obj_", 0);
			String string = sharedPreferences.getString(key, "");
			byte[] imageBytes = Base64.decode(string.getBytes(), Base64.DEFAULT);
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imageBytes);
			return BitmapFactory.decodeStream(byteArrayInputStream);
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.error("json", "读取Bitmap失败");
			return null;
		}
		
	}

	/**
	*必须实现序列接口
	*@author huangbo
	*create at 2016/10/21 14:06
	*/
	public static boolean saveObjectExt(Context context, String key, Serializable obj) {
		try {
			// 保存对象
			SharedPreferences.Editor sharedata = context.getSharedPreferences("shared_obj_", 0).edit();
			sharedata.putString(key, getObjBase64Str(obj));
			return sharedata.commit();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.error("json", "保存对象失败");
			return false;
		}
	}

	/**
	*读取一个对象
	*@author huangbo
	*create at 2016/10/21 14:05
	*/
	public static synchronized Object readObjectExt(Context context, String key) {
		try {
			SharedPreferences sharedPreferences = context.getSharedPreferences("shared_obj_", 0);
			String base64Str = sharedPreferences.getString(key, "");
			byte[] objBytes = Base64.decode(base64Str.getBytes(), Base64.DEFAULT);
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(objBytes);
			ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
			objectInputStream.close();
			return objectInputStream.readObject();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.error("json", "读取对象失败");
			return null;
		}
	}

	/**
	 * 
	 * <b>@Description:<b>得到一个对象的base64字符串<br/>
	 * <b>@param obj <b>@return<b>String<br/>
	 * <b>@Author:<b>ccy<br/>
	 * <b>@Since:<b>2014-7-25-下午5:14:19<br/>
	 */
	public static String getObjBase64Str(Serializable obj) {
		try {
			// 实例化一个ByteArrayOutputStream对象，用来装载压缩后的字节文件。
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			// 然后将得到的字符数据装载到ObjectOutputStream
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			// writeObject 方法负责写入特定类的对象的状态，以便相应的 readObject 方法可以还原它
			objectOutputStream.writeObject(obj);
			// 最后，用Base64.encode将字节文件转换成Base64编码保存在String中
			String objBase64Str = new String(Base64.encode(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
			// 关闭objectOutputStream
			objectOutputStream.close();
			return objBase64Str;
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.error("json", "对象转换base64字符串失败");
			return null;
		}
	}

	/**
	 * 
	 * <b>@Description:<b>根据key来删除<br/>
	 * <b>@param context
	 * <b>@param key
	 * <b>@return<b>boolean<br/>
	 * <b>@Author:<b>ccy<br/>
	 * <b>@Since:<b>2014-8-21-上午11:48:12<br/>
	 */
	public static boolean removeKey(Context context, String key){
		try {
			SharedPreferences settings = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = settings.edit();
			editor.remove(key);
			editor.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}


	public static Long getLastUpdateTime(Context pContext, String pKey, Long pDefauleValue) {
		SharedPreferences oSharedPreferences = pContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
		return oSharedPreferences.getLong(pKey, pDefauleValue);
	}

	public static void setLastUpdateTime(Context pContext, String pKey, Long pValue) {
		SharedPreferences oSharedPreferences = pContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
		oSharedPreferences.edit().putLong(pKey, pValue).commit();
	}

//	public static void getUpdateTime(Context pContext, PullToRefreshListView pListView) {
//		Long oLastUpdateTime = SharePreferenceUtils.getLastUpdateTime(pContext, (String) pListView.getTag(), System.currentTimeMillis());
//		if (oLastUpdateTime != -1) {
//			Date oCurrentDate = new Date(oLastUpdateTime);
//			String oLastUpdate = pContext.getResources().getString(R.string.last_update);
//			SimpleDateFormat oSimpleDateFormat = new SimpleDateFormat(pContext.getString(R.string.date_format), Locale.ENGLISH);
//			String oUpdateTime = oSimpleDateFormat.format(oCurrentDate);
//			pListView.getLoadingLayoutProxy().setLastUpdatedLabel(oLastUpdate + oUpdateTime);
//		}
//	}
}
