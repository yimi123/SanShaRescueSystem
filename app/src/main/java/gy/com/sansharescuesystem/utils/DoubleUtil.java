package gy.com.sansharescuesystem.utils;


import java.text.NumberFormat;

public class DoubleUtil {
    //有带%
    public static String double2Percent(double pAmount, double pAllAmount) {
        //这里的数后面加“D”是表明它是Double类型，否则相除的话取整，无法正常使用
        double oPercent = pAmount / pAllAmount;
        //输出一下，确认你的小数无误
        System.out.println("小数：" + oPercent);
        //获取格式化对象
        NumberFormat nt = NumberFormat.getPercentInstance();
        //设置百分数精确度2即保留两位小数
        nt.setMinimumFractionDigits(2);
        //最后格式化并输出
        return nt.format(oPercent);
    }

    //没有带%
    public static double double2NoPercent(double pAmount, double pAllAmount) {
        //这里的数后面加“D”是表明它是Double类型，否则相除的话取整，无法正常使用
        double oPercent =( pAmount / pAllAmount)*100;
        return oPercent;
    }


}
