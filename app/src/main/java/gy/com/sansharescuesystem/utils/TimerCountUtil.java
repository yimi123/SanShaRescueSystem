package gy.com.sansharescuesystem.utils;


import android.os.CountDownTimer;
import android.widget.Button;

public class TimerCountUtil extends CountDownTimer {
    private Button mBtnTimer;

    public TimerCountUtil(long millisInFuture, long countDownInterval, Button pBtnTimer) {
        super(millisInFuture, countDownInterval);
        this.mBtnTimer = pBtnTimer;
    }

    public TimerCountUtil(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onFinish() {
        mBtnTimer.setClickable(true);
        mBtnTimer.setText("获取验证码");
    }

    @Override
    public void onTick(long arg0) {
        mBtnTimer.setClickable(false);
        mBtnTimer.setText(arg0 / 1000 + "秒后重新获取");
    }
}