package gy.com.sansharescuesystem.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetWorkUtils {
    private NetWorkUtils() {}

//    public static int getNetWorkState(Context context) {
//        WifiManager mWifiManager = (WifiManager)context.getSystemService("wifi");
//        WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
//        int ipAddress = wifiInfo == null?0:wifiInfo.getIpAddress();
//        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService("connectivity");
//        byte state;
//        NetworkInfo info;
//        if(mWifiManager.isWifiEnabled() && ipAddress != 0) {
//            if(connectivity == null) {
//                state = 1;//wifi
//            } else {
//                info = connectivity.getActiveNetworkInfo();
//                if(info == null) {
//                    state = 1;
//                } else if(info.isAvailable()) {
//                    state = 0;
//                } else {
//                    state = 1;
//                }
//            }
//        } else if(connectivity == null) {
//            state = 3;
//        } else {
//            info = connectivity.getActiveNetworkInfo();
//            if(info == null) {
//                state = 3;
//            } else if(info.isAvailable()) {
//                state = 2;
//            } else {
//                state = 3;
//            }
//        }
//
//        return state;
//    }

    /**
     * function:判断有无可用网络
     * @param context
     * @return
     * author:zsf
     * time:2017.07.17
     *
     */
    public static boolean IsHaveInternet(final Context context) {
        try {
            ConnectivityManager manger = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = manger.getActiveNetworkInfo();
            Log.e("info;", info + "..");
            if (info != null) {
                Log.e("info;", info.isConnected() + "..");
            }
            // return (info != null && info.isConnected());
            return (info != null);
        } catch (Exception e) {
            return false;
        }
    }
}

