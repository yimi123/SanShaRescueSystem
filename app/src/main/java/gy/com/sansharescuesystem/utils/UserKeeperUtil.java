package gy.com.sansharescuesystem.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

import gy.com.sansharescuesystem.entity.ClsUser;


public class UserKeeperUtil {
    private static final String PREFERENCES_NAME = "User";
    private static ClsUser mClsUser;
    private static int mUserID;

    public static void SaveUserInfo(Context context, ClsUser user) {
        if (user != null) {
//            int strUserID = user.getUserId();
//            String strToken = user.getToken();
            SharedPreferences pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_APPEND);
            mClsUser = user;
            Editor editor = pref.edit();
//            editor.putInt("UserID", strUserID);
//            editor.putString("token",strToken);
            editor.commit();
        }
    }

    public static void setUserLogout(Context pContext) {
        mClsUser = null;
        mUserID = 0;
        SharedPreferences pref = pContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_APPEND);
        Editor editor = pref.edit();
        editor.putInt("UserID", 0);
        editor.putString("token","");
        editor.commit();
    }

    public static ClsUser getUser(Context context) {
        if (mClsUser == null) {
            SharedPreferences pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_APPEND);
            int strUserID = pref.getInt("UserID", 0);
            String strToken = pref.getString("token", "");
            if (strUserID > 0) {
                mClsUser = new ClsUser();
//                mClsUser.setUserId(strUserID);
//                mClsUser.setToken(strToken);
            }
        }
        return mClsUser;
    }

    public static int getUserID(Context c) {
        if (mUserID == 0) {
            if (mClsUser == null) {
                mClsUser = UserKeeperUtil.getUser(c);
            }
            if (mClsUser != null && !TextUtils.isEmpty(getToken())) {
//                mUserID = mClsUser.getUserId();
            }
        }
        return mUserID;
    }

    public static String getToken() {
        String token = null;
        if (mClsUser != null) {
//            token = mClsUser.getToken();
        }

        return token;
    }

//	public static int getStatus() {
//		if (moClsUserInfo == null) {
//			return 0;
//		}
//		return moClsUserInfo.getStatus();
//	}

//	public static String getDisplayName(Context c) {
//		if (TextUtils.isEmpty(mDisplayName)) {
//			if (moClsUserInfo == null) {
//				moClsUserInfo = getUserInfo(c);
//			}
//			mDisplayName = moClsUserInfo.getDisplayName();
//		}
//		return mDisplayName;
//	}

    public static long getCmdSeq(Context context) {
        ClsUser oClsUser = getUser(context);
        long intSequence = 0, intNextSEQ = 0;
        if (oClsUser != null) {
            intNextSEQ = intSequence + 1;
            SharedPreferences pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_APPEND);
            Editor editor = pref.edit();
            editor.putLong("Sequence", intNextSEQ);
            editor.commit();
        }
        return intSequence;
    }

    // public static void SetCmdSeq(Context context, long pSequence) {
    // ClsUserInfo oClsUserInfo = getUserInfo(context);
    // if (oClsUserInfo != null) {
    // oClsUserInfo.setSequence(pSequence);
    // SharedPreferences pref = context.getSharedPreferences(PREFERENCES_NAME,
    // Context.MODE_APPEND);
    // Editor editor = pref.edit();
    // editor.putLong("Sequence", pSequence);
    // editor.commit();
    // }
    // }

    public static String getUserDBName(Context context) {

        int strUserID = getUserID(context);
        String dbName = "";
        if (strUserID == 0) {
            dbName = "kco.db";
        } else {
            dbName = strUserID + ".db";
        }
        return dbName;
    }
}
