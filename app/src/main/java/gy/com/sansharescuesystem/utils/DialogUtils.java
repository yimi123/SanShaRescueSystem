package gy.com.sansharescuesystem.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import gy.com.sansharescuesystem.R;


public class DialogUtils {
    public static Dialog showCustomDefultDialog(Context pContext, String pTitle, String pMassage, String Submit, String Cancel, View.OnClickListener pOKOnClickListener, View.OnClickListener pCancelOnClickListener, View.OnClickListener pExitOnClickListener) {
        Dialog oDialog;
        ContextThemeWrapper themedContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            themedContext = new ContextThemeWrapper(pContext, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        } else {
            themedContext = new ContextThemeWrapper(pContext, android.R.style.Theme_Light_NoTitleBar);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);

        View view = LayoutInflater.from(pContext).inflate(R.layout.dialog_default, null, false);
        builder.setView(view);
        TextView oDialogTitle = (TextView) view.findViewById(R.id.dialog_title);
        TextView oDialogMassage = (TextView) view.findViewById(R.id.dialog_massage);
        TextView oDialogSubmit = (TextView) view.findViewById(R.id.dialog_submit);
        TextView oDialogCancel = (TextView) view.findViewById(R.id.dialog_cancel);
        ImageView oDialogExit = (ImageView) view.findViewById(R.id.dialog_exit);
        try {
            oDialogTitle.setVisibility(pTitle.equals("") ? View.GONE : View.VISIBLE);
            oDialogMassage.setVisibility(pMassage.equals("") ? View.GONE : View.VISIBLE);
            oDialogSubmit.setVisibility(Submit.equals("") ? View.GONE : View.VISIBLE);
            oDialogCancel.setVisibility(Cancel.equals("") ? View.GONE : View.VISIBLE);

            oDialogTitle.setText(pTitle);
            oDialogMassage.setText(pMassage);
            oDialogSubmit.setText(Submit);
            oDialogCancel.setText(Cancel);

            if (pOKOnClickListener != null) {
                oDialogSubmit.setOnClickListener(pOKOnClickListener);
            }
            if (pCancelOnClickListener != null) {
                oDialogCancel.setOnClickListener(pCancelOnClickListener);
            }
            if (pExitOnClickListener != null) {
                oDialogExit.setOnClickListener(pExitOnClickListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        builder.setCancelable(false);
        oDialog = builder.create();
        oDialog.show();
        return oDialog;
    }

    public static Dialog showCustomNoCancelBarDialog(Context pContext, String pTitle, String pMassage, String Submit, String Cancel, View.OnClickListener pOKOnClickListener, View.OnClickListener pExitOnClickListener) {
        Dialog oDialog = null;
        ContextThemeWrapper themedContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            themedContext = new ContextThemeWrapper(pContext, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        } else {
            themedContext = new ContextThemeWrapper(pContext, android.R.style.Theme_Light_NoTitleBar);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);

        View view = LayoutInflater.from(pContext).inflate(R.layout.dialog_default, null, false);
        builder.setView(view);
        TextView oDialogTitle = (TextView) view.findViewById(R.id.dialog_title);
        TextView oDialogMassage = (TextView) view.findViewById(R.id.dialog_massage);
        TextView oDialogSubmit = (TextView) view.findViewById(R.id.dialog_submit);
        TextView oDialogCancel = (TextView) view.findViewById(R.id.dialog_cancel);
        ImageView oDialogExit = (ImageView) view.findViewById(R.id.dialog_exit);
        try {
            oDialogTitle.setVisibility(pTitle.equals("") ? View.GONE : View.VISIBLE);
            oDialogMassage.setVisibility(pMassage.equals("") ? View.GONE : View.VISIBLE);
            oDialogSubmit.setVisibility(Submit.equals("") ? View.GONE : View.VISIBLE);
            oDialogCancel.setVisibility(Cancel.equals("") ? View.GONE : View.VISIBLE);

            oDialogTitle.setText(pTitle);
            oDialogMassage.setText(pMassage);
            oDialogSubmit.setText(Submit);
            oDialogCancel.setText(Cancel);

            if (pOKOnClickListener != null) {
                oDialogSubmit.setOnClickListener(pOKOnClickListener);
            }
            if (pExitOnClickListener != null) {
                oDialogExit.setOnClickListener(pExitOnClickListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        builder.setCancelable(false);
        oDialog = builder.create();
        oDialog.show();
        return oDialog;
    }



    public static Dialog showLoadingBarDialog(Context pContext) {
        Dialog oDialog;
        ContextThemeWrapper themedContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            themedContext = new ContextThemeWrapper(pContext, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        } else {
            themedContext = new ContextThemeWrapper(pContext, android.R.style.Theme_Light_NoTitleBar);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        View view = LayoutInflater.from(pContext).inflate(R.layout.dialog_loadingbar, null, false);
        builder.setView(view);
        builder.setCancelable(false);
        oDialog = builder.create();
        oDialog.show();
        setDialogWindowAttr(oDialog,pContext);
        return oDialog;
    }

    //在dialog.show()之后调用
    public static void setDialogWindowAttr(Dialog dlg, Context pContext){
        Window window = dlg.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.CENTER;
        int dialogWidth = DisplayUtils.dip2px(pContext, 300);
        int dialogHeight = DisplayUtils.dip2px(pContext,300);
        lp.width = dialogWidth;//宽高可设置具体大小;
        lp.height = dialogHeight;
        dlg.getWindow().setAttributes(lp);
    }

    public static Dialog createLoadingDialog(Context context, String msg,boolean isCancel) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.dialog_loading, null);// 得到加载view
        LinearLayout layout = (LinearLayout) v
                .findViewById(R.id.dialog_loading_view);// 加载布局
        TextView tipTextView = (TextView) v.findViewById(R.id.tipTextView);// 提示文字
        tipTextView.setText(msg);// 设置加载信息

        Dialog loadingDialog = new Dialog(context, R.style.MyDialogStyle);// 创建自定义样式dialog
        loadingDialog.setCancelable(isCancel); // 是否可以按“返回键”消失
        loadingDialog.setCanceledOnTouchOutside(false); // 点击加载框以外的区域
        loadingDialog.setContentView(layout, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));// 设置布局

         //将显示Dialog的方法封装在这里面
        Window window = loadingDialog.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp);
        window.setWindowAnimations(R.style.PopWindowAnimStyle);
        loadingDialog.show();

        return loadingDialog;
    }
    public static void closeDialog(Dialog mDialogUtils) {
        if (mDialogUtils != null && mDialogUtils.isShowing()) {
            mDialogUtils.dismiss();
        }
    }

}
