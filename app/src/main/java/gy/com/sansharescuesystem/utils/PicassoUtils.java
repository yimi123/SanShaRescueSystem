package gy.com.sansharescuesystem.utils;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import gy.com.sansharescuesystem.R;


/**
*图片下载工具类
*@author huangbo
*create at 2016/10/21 14:22
*/
public class PicassoUtils {
    public static void setImageUrl(Context context, String imageUrl, ImageView view) {
        Picasso.with(context).load(imageUrl)
                .placeholder(R.drawable.logo_default)
                .error(R.drawable.logo_default)
                .into(view);
    }
//
//    public static void setImageUrl(Context context, String imageUrl, Target target) {
//        Picasso.with(context).load(imageUrl)
//                .placeholder(R.mipmap.ic_launcher)
//                .error(R.mipmap.ic_launcher)
//                .into(target);
//    }

}
