package gy.com.sansharescuesystem.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreferenceUtils {
    private final static String CONFIG = "config";
    public static Long getLastUpdateTime(Context pContext, String pKey, Long pDefauleValue) {
        SharedPreferences oSharedPreferences = pContext.getSharedPreferences(CONFIG, Context.MODE_PRIVATE);
        return oSharedPreferences.getLong(pKey, pDefauleValue);
    }

    public static void setLastUpdateTime(Context pContext, String pKey, Long pValue) {
        SharedPreferences oSharedPreferences = pContext.getSharedPreferences(CONFIG, Context.MODE_PRIVATE);
        oSharedPreferences.edit().putLong(pKey, pValue).commit();
    }

//    public static void getUpdateTime(Context pContext, PullToRefreshListView pListView) {
//        Long oLastUpdateTime = SharePreferenceUtils.getLastUpdateTime(pContext, (String) pListView.getTag(), System.currentTimeMillis());
//        if (oLastUpdateTime != -1) {
//            Date oCurrentDate = new Date(oLastUpdateTime);
//            String oLastUpdate = pContext.getResources().getString(R.string.last_update);
//            SimpleDateFormat oSimpleDateFormat = new SimpleDateFormat(pContext.getString(R.string.date_format), Locale.ENGLISH);
//            String oUpdateTime = oSimpleDateFormat.format(oCurrentDate);
//            pListView.getLoadingLayoutProxy().setLastUpdatedLabel(oLastUpdate + oUpdateTime);
//        }
//    }


    /**
     *
     *function:获取保存的SharedPreferences
     * @return
     * author:zsf
     * time:2017.07.17
     */
    public static String getStringPerferences(Context context, String name,
                                              String defValues) {
        SharedPreferences preferences = context.getSharedPreferences(
                CONFIG, Context.MODE_PRIVATE);
        synchronized (preferences) {
            return preferences.getString(name, defValues);
        }
    }

    /*
     * function: 设置配置参数
     * author:zsf
     * time:2017.07.17
     */
    public static void setStringSave(Context context, String name, String values) {
        SharedPreferences preferences = context.getSharedPreferences(
                CONFIG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(name, values);
        editor.commit();
    }

    /**
     *
     *function:获取保存的SharedPreferences
     * @return
     * author:zsf
     * time:2017.07.17
     */
    public static int getIntPerferences(Context context, String name,
                                              int defValues) {
        SharedPreferences preferences = context.getSharedPreferences(
                CONFIG, Context.MODE_PRIVATE);
        synchronized (preferences) {
            return preferences.getInt(name, defValues);
        }
    }

    /*
     * function: 设置配置参数
     * author:zsf
     * time:2017.07.17
     */
    public static void setIntSave(Context context, String name, int values) {
        SharedPreferences preferences = context.getSharedPreferences(
                CONFIG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(name, values);
        editor.commit();
    }

    /**
     *
     *function:获取保存的SharedPreferences
     * @return
     * author:zsf
     * time:2017.07.17
     */
    public static boolean getBooleanPerferences(Context context, String name,
                                        boolean defValues) {
        SharedPreferences preferences = context.getSharedPreferences(
                CONFIG, Context.MODE_PRIVATE);
        synchronized (preferences) {
            return preferences.getBoolean(name, defValues);
        }
    }

    /*
     * function: 设置配置参数
     * author:zsf
     * time:2017.07.17
     */
    public static void setBooleanSave(Context context, String name, boolean values) {
        SharedPreferences preferences = context.getSharedPreferences(
                CONFIG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(name, values);
        editor.commit();
    }



}
