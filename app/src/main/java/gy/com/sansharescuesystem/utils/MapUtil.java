package gy.com.sansharescuesystem.utils;

import com.baidu.mapapi.model.LatLng;

/**
 * Created by 陈秋霖 on 2017-07-27.
 */

public class MapUtil {
    private static final double EARTH_RADIUS = 6378.137;
    private static double rad(double d){
        return d*Math.PI/180.0;
    }
    public static float getZoom(double distance){
        float zoom = 0;
        if(distance<4000){
            zoom=11.0f;
        }else if(distance>=4000&&distance<8000){
            zoom = 10.0f;
        }else if(distance>=8000&&distance<200000){
            zoom = 9.0f;
        }else if(distance>=200000&&distance<400000){
            zoom = 8.0f;
        }else if(distance>400000&&distance<=800000){
            zoom = 7.0f;
        }else if(distance>=800000&&distance<1000000){
            zoom = 6.0f;
        }else if(distance>=1000000&&distance<2000000){
            zoom = 5.0f;
        }else if(distance>=2000000&&distance<4000000){
            zoom = 4.0f;
        }else if(distance>=4000000&&distance<8000000){
            zoom = 3.0f;
        }
        return zoom;
    }
    public static double getDistance(double mCurrentLat, double mCurrentLon, double mTargetLat, double mTargetLon) {
        double radLat1 = rad(mCurrentLat);
        double radLat2 = rad(mTargetLat);
        double a = radLat1-radLat2;
        double b = rad(mCurrentLon-mTargetLon);
        double s = 2*Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2)+
                Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
        s = s*EARTH_RADIUS;
        s = Math.round(s*10000)/10000;
        return s*1000;
    }
    public static float getDirection(LatLng point1, LatLng nextP) {
        float direction=0;
        double y = nextP.latitude- point1.latitude;
        double x = nextP.longitude-point1.longitude;
        if(x>=0&&y>0){//第一象限
            direction = Float.parseFloat((Math.toDegrees(Math.atan2(y,x)))+"")+270;
        }else if(x<0&&y>0){//第四象限
            direction = Float.parseFloat((Math.toDegrees(Math.atan2(y,x)))+"")-90;
        }else if(x>=0&&y<=0){//第二象限
            direction = Float.parseFloat((Math.toDegrees(Math.atan2(y,x)))+"")+270;
        }else if(x<0&&y<=0){//第三象限
            direction = Float.parseFloat((Math.toDegrees(Math.atan2(y,x)))+"")-90;
        }
        return direction;
    }
}
