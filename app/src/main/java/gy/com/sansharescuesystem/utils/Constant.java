package gy.com.sansharescuesystem.utils;


public class Constant {
    public static final String RESCUE_PAUSE = "rescue_pause";
    public static final String RESCUE_STOP = "rescue_stop";
    public static final String RESCUE_RESUME = "rescue_resume";
    public static final String RESCUE_DESTROY = "rescue_destroy";
    public static final String TO_HISTORY_PAGE = "to_history_page";
    public static final String TO_RESCUE_PAGE = "to_rescue_page";
    public static final String TO_COUNT_PAGE = "to_count_page";

    public static final int FLAG_HISTORY = 1;
    public static final int FLAG_RESCUE = 2;
    public static final int FLAG_COUNT = 3;
}
