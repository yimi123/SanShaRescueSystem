/*
 * File Name: LogUtils.java 
 * History:
 * Created by mwqi on 2014-4-4
 */
package gy.com.sansharescuesystem.utils;

import android.text.TextUtils;
import android.util.Log;

import java.util.List;

/**
*日志输出控制类
*@author huangbo
*create at 2016/10/21 9:51
*/
public class LogUtils {
    /**
     * 日志输出级别V
     */
    private static final int LEVEL_VERBOSE = 1;
    /**
     * 日志输出级别D
     */
    private static final int LEVEL_DEBUG = 2;
    /**
     * 日志输出级别I
     */
    private static final int LEVEL_INFO = 3;
    /**
     * 日志输出级别W
     */
    private static final int LEVEL_WARN = 4;
    /**
     * 日志输出级别E
     */
    private static final int LEVEL_ERROR = 5;

    /**
     * 日志输出时的TAG
     */
    private static final String mTag = "rick";
    /**
     * 是否允许输出log
     */
    private static final int mDebuggable = LEVEL_ERROR;

    /**
     * 用于记时的变量
     */
    private static long mTimestamp = 0;
    /**
     * 写文件的锁对象
     */
    private static final Object mLogLock = new Object();
    private final static boolean isInputNow  = true;
    /**
     * 以级别为 v 的形式输出LOG
     */
    public static void verbose(String msg) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_VERBOSE) {
                Log.v(mTag, msg);
            }
        }

    }

    /**
     * 以级别为 d 的形式输出LOG
     */
    public static void debug(String msg) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_DEBUG) {
                Log.d(mTag, msg);
            }
        }

    }

    /**
     * 以级别为 d 的形式输出LOG
     */
    public static void debug(String tag, String msg) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_DEBUG) {
                Log.d(tag, msg);
            }
        }

    }
    /**
     * 以级别为 i 的形式输出LOG
     */
    private static void info(String msg) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_INFO) {
            Log.i(mTag, msg);
        }}

    }

    /**
     * 以级别为 i 的形式输出LOG
     */
    public static void info(String tag, String msg) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_INFO) {
                Log.i(tag, msg);
            }
        }

    }

    /**
     * 以级别为 w 的形式输出LOG
     */
    public static void warn(String msg) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_WARN) {
                Log.w(mTag, msg);
            }
        }

    }

    /**
     * 以级别为 w 的形式输出Throwable
     */
    public static void warn(Throwable tr) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_WARN) {
                Log.w(mTag, "", tr);
            }
        }
    }

    /**
     * 以级别为 w 的形式输出LOG信息和Throwable
     */
    public static void warn(String msg, Throwable tr) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_WARN && null != msg) {
                Log.w(mTag, msg, tr);
            }
        }

    }

    /**
     * 以级别为 e 的形式输出LOG
     */
    public static void error(String msg) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_ERROR) {
                Log.e(mTag, msg);
            }
        }
    }

    /**
     * 以级别为 e 的形式输出LOG
     */
    public static void error(String tag, String msg) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_ERROR) {
                Log.e(tag, msg);
            }
        }

    }

    /**
     * 以级别为 e 的形式输出Throwable
     */
    public static void error(Throwable tr) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_ERROR) {
                Log.e(mTag, "", tr);
            }
        }

    }

    /**
     * 以级别为 e 的形式输出LOG信息和Throwable
     */
    public static void error(String msg, Throwable tr) {
        if(isInputNow){
            if (mDebuggable >= LEVEL_ERROR && null != msg) {
                Log.e(mTag, msg, tr);
            }
        }

    }

    /**
     * 把Log存储到文件中
     *
     * @param log  需要存储的日志
     * @param path 存储路径
     */
    public static void log2File(String log, String path) {
        log2File(log, path, true);
    }

    private static void log2File(String log, String path, boolean append) {
        synchronized (mLogLock) {
            FileUtils.writeFile(log + "\r\n", path, append);
        }
    }

    /**
     * 以级别为 e 的形式输出msg信息,附带时间戳，用于输出一个时间段起始点
     *
     * @param msg 需要输出的msg
     */
    public static void msgStartTime(String msg) {
        if(isInputNow){
            mTimestamp = System.currentTimeMillis();
            if (!TextUtils.isEmpty(msg)) {
                error("[Started：" + mTimestamp + "]" + msg);
            }
        }

    }

    /**
     * 以级别为 e 的形式输出msg信息,附带时间戳，用于输出一个时间段结束点* @param msg 需要输出的msg
     */
    public static void msgEndTime(String msg) {
        if(isInputNow){
            long currentTime = System.currentTimeMillis();
            long elapsedTime = currentTime - mTimestamp;
            mTimestamp = currentTime;
            error("[Elapsed：" + elapsedTime + "]" + msg);
        }

    }

    public static <T> void printList(List<T> list) {
        if(isInputNow){
            if (list == null || list.size() < 1) {
                return;
            }
            int size = list.size();
            info("---begin---");
            for (int i = 0; i < size; i++) {
                info(i + ":" + list.get(i).toString());
            }
            info("---end---");
        }
    }

    public static <T> void printArray(T[] array) {
        if(isInputNow){
            if (array == null || array.length < 1) {
                return;
            }
            int length = array.length;
            info("---begin---");
            for (int i = 0; i < length; i++) {
                info(i + ":" + array[i].toString());
            }
            info("---end---");
        }

    }


}
