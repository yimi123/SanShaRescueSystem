package gy.com.sansharescuesystem.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gy.com.sansharescuesystem.R;
import gy.com.sansharescuesystem.callback.SelectSonFragmentCallBack;
import gy.com.sansharescuesystem.utils.CloseKeybordUtils;



public abstract class BaseFragment extends Fragment {
    protected Context mContext;
    protected Activity mActivity;
    protected View mView;
    protected FragmentManager mManager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mActivity = getActivity();
        mManager = getActivity().getSupportFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        mView = onCreateView(inflater);
//        initData();
        initView();
        initData();
        setWidgetListener();
        return mView;
    }

    protected abstract View onCreateView(LayoutInflater inflater);

    protected abstract void initView();

    protected void initData(){

    }
    protected abstract void setWidgetListener();

    protected View findViewById(int pViewId){
        return mView.findViewById(pViewId);
    }

    protected boolean isBackPressed(){return false;}

    protected void onBackPressed(){};

    protected Fragment getCurrentFragment(int pContentId){

        return mManager.findFragmentById(pContentId);
    }
    protected Fragment getFragmentByTag(String pTag){
        return mManager.findFragmentByTag(pTag);
    }
    protected void showFragment(Fragment pFromFragment, BaseFragment pToFragment, SelectSonFragmentCallBack pSelectSonFragmentCallBack,int pFragmentLayoutId){
        CloseKeybordUtils.closeKeyboard(mActivity);
        if(!pToFragment.isAdded()){
            FragmentTransaction fragmentTransaction = mManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.fragment_tab_in,R.anim.fragment_tab_out);
            fragmentTransaction.hide(pFromFragment).add(pFragmentLayoutId,pToFragment,getClass().getSimpleName());
            fragmentTransaction.commit();
            if(pSelectSonFragmentCallBack!=null){
                pSelectSonFragmentCallBack.selectFragment(pToFragment);
            }
        }else{
            FragmentTransaction fragmentTransaction = mManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.fragment_tab_in,R.anim.fragment_tab_out);
            fragmentTransaction.remove(pFromFragment).show(pToFragment);
            fragmentTransaction.commit();
            if(pSelectSonFragmentCallBack!=null){
                pSelectSonFragmentCallBack.selectFragment(pToFragment);
            }
        }
    }
    protected void showFragment(BaseFragment fragment,int fragmentId){
        CloseKeybordUtils.closeKeyboard(mActivity);
        FragmentTransaction fragmentTransaction = mManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fragment_tab_in,R.anim.fragment_tab_out);
        fragmentTransaction.replace(fragmentId,fragment,fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
