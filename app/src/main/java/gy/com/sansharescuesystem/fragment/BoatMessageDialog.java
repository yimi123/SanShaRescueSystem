package gy.com.sansharescuesystem.fragment;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import gy.com.sansharescuesystem.R;
import gy.com.sansharescuesystem.entity.ShipDataBean;

/**
 * Created by 陈秋霖 on 2017-08-18.
 */

public class BoatMessageDialog extends DialogFragment implements View.OnClickListener{
    private ImageButton mIbClose;
    private TextView mTvBoatSerial;
    private TextView mTvTeamNum;
    private TextView mTvPeople;
    private TextView mTvPhoneNum;
    private TextView mTvShipNum;
    private TextView mTvShipPaiNum;
    private TextView mTvLora;
    private TextView mTvShipType;
    private TextView mTvDepartment;
    //单例
    private static BoatMessageDialog instance;
    private static Object TAG = new Object();
    public BoatMessageDialog(){}
    public static BoatMessageDialog getInstance(){
        if(instance==null){
            synchronized (TAG){
                if(instance==null){
                    instance = new BoatMessageDialog();
                }
            }
        }
        return instance;
    }
    private int width;
    private int height;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyDialogFrame);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_boat_message,null);
        initView(view);
        initData();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        width = dm.widthPixels;
        height = dm.heightPixels;
        Window mWindow = getDialog().getWindow();
        WindowManager.LayoutParams lp = mWindow.getAttributes();
        lp.dimAmount =0f;
        lp.alpha = 1.0f;
        lp.dimAmount = 1.0f;
        mWindow.setAttributes(lp);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        //设置全屏
        getDialog().getWindow().setLayout(width,height);
    }

    private void initView(View view) {
        mTvBoatSerial = (TextView) view.findViewById(R.id.boat_message_serial);
        mTvTeamNum = (TextView) view.findViewById(R.id.boat_message_team_num);
        mTvPeople = (TextView) view.findViewById(R.id.boat_message_people);
        mTvPhoneNum = (TextView) view.findViewById(R.id.boat_message_phone_num);
        mTvShipNum = (TextView) view.findViewById(R.id.boat_message_ship_num);
        mTvShipPaiNum = (TextView) view.findViewById(R.id.boat_message_ship_pai_num);
        mTvLora = (TextView) view.findViewById(R.id.boat_message_station);
        mTvShipType = (TextView) view.findViewById(R.id.boat_message_ship_type);
        mTvDepartment = (TextView) view.findViewById(R.id.boat_message_belong_org);
        mIbClose = (ImageButton) view.findViewById(R.id.boat_message_close);
        mIbClose.setOnClickListener(this);
    }

    private void initData() {

    }
    public void setData(ShipDataBean bean){
        mTvBoatSerial.setText(bean.getSerial());
        mTvTeamNum.setText(bean.getShip());
        mTvPeople.setText(bean.getName());
        mTvPhoneNum.setText(bean.getPhone());
        mTvShipNum.setText(bean.getDiy());
        mTvShipPaiNum.setText(bean.getLic());
        mTvLora.setText(bean.getLora());
        mTvShipType.setText(bean.getType());
        mTvDepartment.setText(bean.getDepart());
    }

    @Override
    public void onClick(View v) {
        this.dismiss();
    }
}
