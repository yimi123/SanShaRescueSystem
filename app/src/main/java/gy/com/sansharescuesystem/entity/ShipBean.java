package gy.com.sansharescuesystem.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 陈秋霖 on 2017-08-29.
 */

public class ShipBean implements Parcelable{
    private int flag;
    private String msg;
    private String time;
    private List<ShipDataBean> data;

    protected ShipBean(Parcel in) {
        flag = in.readInt();
        msg = in.readString();
        time = in.readString();
        data = in.createTypedArrayList(ShipDataBean.CREATOR);
    }

    public static final Creator<ShipBean> CREATOR = new Creator<ShipBean>() {
        @Override
        public ShipBean createFromParcel(Parcel in) {
            return new ShipBean(in);
        }

        @Override
        public ShipBean[] newArray(int size) {
            return new ShipBean[size];
        }
    };

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<ShipDataBean> getData() {
        return data;
    }

    public void setData(List<ShipDataBean> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(flag);
        dest.writeString(msg);
        dest.writeString(time);
        dest.writeTypedList(data);
    }
}
