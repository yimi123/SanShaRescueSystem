package gy.com.sansharescuesystem.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 陈秋霖 on 2017-07-26.
 */

public class RescueListResult implements Parcelable{
    private Integer flag;
    private String msg;
    private String pages;
    private String current;
    private String total;
    private String time;
    private List<RescueListResData> data;

    public RescueListResult() {
    }

    public RescueListResult(Integer flag, String msg, String pages, String current, String total, String time, List<RescueListResData> data) {
        this.flag = flag;
        this.msg = msg;
        this.pages = pages;
        this.current = current;
        this.total = total;
        this.time = time;
        this.data = data;
    }

    protected RescueListResult(Parcel in) {
        flag = in.readInt();
        msg = in.readString();
        pages = in.readString();
        current = in.readString();
        total = in.readString();
        time = in.readString();
    }

    public static final Creator<RescueListResult> CREATOR = new Creator<RescueListResult>() {
        @Override
        public RescueListResult createFromParcel(Parcel in) {
            return new RescueListResult(in);
        }

        @Override
        public RescueListResult[] newArray(int size) {
            return new RescueListResult[size];
        }
    };

    @Override
    public String toString() {
        return "RescueListResult{" +
                "flag='" + flag + '\'' +
                ", msg='" + msg + '\'' +
                ", pages='" + pages + '\'' +
                ", current='" + current + '\'' +
                ", total='" + total + '\'' +
                ", time='" + time + '\'' +
                ", data=" + data +
                '}';
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<RescueListResData> getData() {
        return data;
    }

    public void setData(List<RescueListResData> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(flag);
        dest.writeString(msg);
        dest.writeString(pages);
        dest.writeString(current);
        dest.writeString(total);
        dest.writeString(time);
    }
}
