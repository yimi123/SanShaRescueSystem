package gy.com.sansharescuesystem.entity;

import java.util.List;

/**
 * Created by lenovo on 2017-07-26.
 */
public class HeatTotalBean {

        private String flag;


        private String msg;
        private String time;
        private List<HeatDataBean> data;


    public List<HeatDataBean> getData() {
        return data;
    }

    public void setData(List<HeatDataBean> data) {
        this.data = data;
    }

    public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }


    }




