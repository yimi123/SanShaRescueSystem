package gy.com.sansharescuesystem.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 陈秋霖 on 2017-07-24.
 */

public class HistoryOrbit implements Parcelable{
    private String flag;
    private String msg;
    private Integer page;
    private Integer current;
    private String total;
    private String time;
    private List<HistoryOrbitData> data;

    public HistoryOrbit() {
    }

    public HistoryOrbit(String flag, String msg, int page, int current, String total, String time, List<HistoryOrbitData> data) {
        this.flag = flag;
        this.msg = msg;
        this.page = page;
        this.current = current;
        this.total = total;
        this.time = time;
        this.data = data;
    }

    protected HistoryOrbit(Parcel in) {
        flag = in.readString();
        msg = in.readString();
        page = in.readInt();
        current = in.readInt();
        total = in.readString();
        time = in.readString();
        data = in.createTypedArrayList(HistoryOrbitData.CREATOR);
    }

    public static final Creator<HistoryOrbit> CREATOR = new Creator<HistoryOrbit>() {
        @Override
        public HistoryOrbit createFromParcel(Parcel in) {
            return new HistoryOrbit(in);
        }

        @Override
        public HistoryOrbit[] newArray(int size) {
            return new HistoryOrbit[size];
        }
    };

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<HistoryOrbitData> getData() {
        return data;
    }

    public void setData(List<HistoryOrbitData> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "HistoryOrbit{" +
                "flag='" + flag + '\'' +
                ", msg='" + msg + '\'' +
                ", page=" + page +
                ", current=" + current +
                ", total='" + total + '\'' +
                ", time='" + time + '\'' +
                ", data=" + data +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(flag);
        dest.writeString(msg);
        dest.writeInt(page);
        dest.writeInt(current);
        dest.writeString(total);
        dest.writeString(time);
        dest.writeTypedList(data);
    }
}
