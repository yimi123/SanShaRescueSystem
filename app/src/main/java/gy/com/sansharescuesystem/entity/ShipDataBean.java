package gy.com.sansharescuesystem.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 陈秋霖 on 2017-08-29.
 */

public class ShipDataBean implements Parcelable{
    private String serial;
    private String lora;
    private String ship;
    private String name;
    private String phone;
    private String diy;
    private String type;
    private String lic;
    private String depart;

    protected ShipDataBean(Parcel in) {
        serial = in.readString();
        lora = in.readString();
        ship = in.readString();
        name = in.readString();
        phone = in.readString();
        diy = in.readString();
        type = in.readString();
        lic = in.readString();
        depart = in.readString();
    }

    public static final Creator<ShipDataBean> CREATOR = new Creator<ShipDataBean>() {
        @Override
        public ShipDataBean createFromParcel(Parcel in) {
            return new ShipDataBean(in);
        }

        @Override
        public ShipDataBean[] newArray(int size) {
            return new ShipDataBean[size];
        }
    };

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getLora() {
        return lora;
    }

    public void setLora(String lora) {
        this.lora = lora;
    }

    public String getShip() {
        return ship;
    }

    public void setShip(String ship) {
        this.ship = ship;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDiy() {
        return diy;
    }

    public void setDiy(String diy) {
        this.diy = diy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLic() {
        return lic;
    }

    public void setLic(String lic) {
        this.lic = lic;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    @Override
    public String toString() {
        return "ShipDataBean{" +
                "serial='" + serial + '\'' +
                ", lora='" + lora + '\'' +
                ", ship='" + ship + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", diy='" + diy + '\'' +
                ", type='" + type + '\'' +
                ", lic='" + lic + '\'' +
                ", depart='" + depart + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serial);
        dest.writeString(lora);
        dest.writeString(ship);
        dest.writeString(name);
        dest.writeString(phone);
        dest.writeString(diy);
        dest.writeString(type);
        dest.writeString(lic);
        dest.writeString(depart);
    }
}
