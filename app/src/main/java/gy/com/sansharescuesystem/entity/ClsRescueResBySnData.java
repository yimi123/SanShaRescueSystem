package gy.com.sansharescuesystem.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 陈秋霖 on 2017-07-19.
 */

public class ClsRescueResBySnData implements Parcelable{
    private String id;
    private String serial;
    private String log;
    private String lat;
    private String status;
    private String c_time;
    private String e_time;
    private String j_time;
    private String actor1;
    private String operation_end;
    private String time;
    private String over_type;
    private String num;

    protected ClsRescueResBySnData(Parcel in) {
        id = in.readString();
        serial = in.readString();
        log = in.readString();
        lat = in.readString();
        status = in.readString();
        c_time = in.readString();
        e_time = in.readString();
        j_time = in.readString();
        actor1 = in.readString();
        operation_end = in.readString();
        time = in.readString();
        over_type = in.readString();
        num = in.readString();
    }

    public static final Creator<ClsRescueResBySnData> CREATOR = new Creator<ClsRescueResBySnData>() {
        @Override
        public ClsRescueResBySnData createFromParcel(Parcel in) {
            return new ClsRescueResBySnData(in);
        }

        @Override
        public ClsRescueResBySnData[] newArray(int size) {
            return new ClsRescueResBySnData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getC_time() {
        return c_time;
    }

    public void setC_time(String c_time) {
        this.c_time = c_time;
    }

    public String getE_time() {
        return e_time;
    }

    public void setE_time(String e_time) {
        this.e_time = e_time;
    }

    public String getJ_time() {
        return j_time;
    }

    public void setJ_time(String j_time) {
        this.j_time = j_time;
    }

    public String getActor1() {
        return actor1;
    }

    public void setActor1(String actor1) {
        this.actor1 = actor1;
    }

    public String getOperation_end() {
        return operation_end;
    }

    public void setOperation_end(String operation_end) {
        this.operation_end = operation_end;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getOver_type() {
        return over_type;
    }

    public void setOver_type(String over_type) {
        this.over_type = over_type;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "ClsRescueResBySnData{" +
                "id='" + id + '\'' +
                ", serial='" + serial + '\'' +
                ", log='" + log + '\'' +
                ", lat='" + lat + '\'' +
                ", status='" + status + '\'' +
                ", c_time='" + c_time + '\'' +
                ", e_time='" + e_time + '\'' +
                ", j_time='" + j_time + '\'' +
                ", actor1='" + actor1 + '\'' +
                ", operation_end='" + operation_end + '\'' +
                ", time='" + time + '\'' +
                ", over_type='" + over_type + '\'' +
                ", num='" + num + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(serial);
        dest.writeString(log);
        dest.writeString(lat);
        dest.writeString(status);
        dest.writeString(c_time);
        dest.writeString(e_time);
        dest.writeString(j_time);
        dest.writeString(actor1);
        dest.writeString(operation_end);
        dest.writeString(time);
        dest.writeString(over_type);
        dest.writeString(num);
    }
}
