package gy.com.sansharescuesystem.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 陈秋霖 on 2017-07-24.
 */

public class HistoryOrbitData implements Parcelable{
    private String id;
    private String log;
    private String lat;
    private String jing;
    private String wei;
    private String energy;
    private String ctime;

    public HistoryOrbitData() {
    }

    public HistoryOrbitData(String id, String log, String lat, String jing, String wei, String energy, String ctime) {
        this.id = id;
        this.log = log;
        this.lat = lat;
        this.jing = jing;
        this.wei = wei;
        this.energy = energy;
        this.ctime = ctime;
    }

    protected HistoryOrbitData(Parcel in) {
        id = in.readString();
        log = in.readString();
        lat = in.readString();
        jing = in.readString();
        wei = in.readString();
        energy = in.readString();
        ctime = in.readString();
    }

    public static final Creator<HistoryOrbitData> CREATOR = new Creator<HistoryOrbitData>() {
        @Override
        public HistoryOrbitData createFromParcel(Parcel in) {
            return new HistoryOrbitData(in);
        }

        @Override
        public HistoryOrbitData[] newArray(int size) {
            return new HistoryOrbitData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getJing() {
        return jing;
    }

    public void setJing(String jing) {
        this.jing = jing;
    }

    public String getWei() {
        return wei;
    }

    public void setWei(String wei) {
        this.wei = wei;
    }

    public String getEnergy() {
        return energy;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    @Override
    public String toString() {
        return "HistoryOrbitData{" +
                "id='" + id + '\'' +
                ", log='" + log + '\'' +
                ", lat='" + lat + '\'' +
                ", jing='" + jing + '\'' +
                ", wei='" + wei + '\'' +
                ", energy='" + energy + '\'' +
                ", ctime='" + ctime + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(log);
        dest.writeString(lat);
        dest.writeString(jing);
        dest.writeString(wei);
        dest.writeString(energy);
        dest.writeString(ctime);
    }
}
