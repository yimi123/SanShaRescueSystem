package gy.com.sansharescuesystem.entity;

/**
 * Created by zsf 2017-07-18.
 */
public class CountBean {
    private String flag;
    private String msg;
    private String time;
    //data必须和json数据中的对象名一样，否则解析不出数据
    public CountDataBean data;
    public static class CountDataBean{
        // 救援实施次数
        public String all;
        // 救援成功次数
        public String success;
        // 平均接警时长(单位秒)
        public String get_avg;
        //  平均救援时长(单位秒)
        public String help_avg;
        public String getAll() {
            return all;
        }

        public void setAll(String all) {
            this.all = all;
        }

        public String getSuccess() {
            return success;
        }

        public void setSuccess(String success) {
            this.success = success;
        }

        public String getGet_avg() {
            return get_avg;
        }

        public void setGet_avg(String get_avg) {
            this.get_avg = get_avg;
        }

        public String getHelp_avg() {
            return help_avg;
        }

        public void setHelp_avg(String help_avg) {
            this.help_avg = help_avg;
        }

    }
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMsg() {

        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFlag() {

        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public CountDataBean getCountDataBean() {
        return data;
    }

    public void setCountDataBean(CountDataBean countDataBean) {
        this.data = countDataBean;
    }

    @Override
    public String toString() {
        return "CountBean{" +
                "flag='" + flag + '\'' +
                ", msg='" + msg + '\'' +
                ", time='" + time + '\'' +
                ", countDataBean=" + data +
                '}';
    }
}
