package gy.com.sansharescuesystem.entity;

/**
 * Created by 陈秋霖 on 2017-07-12.
 */

public class ClsUser {
    private String mLoginName;
    private String mPassWord;

    public String getmLoginName() {
        return mLoginName;
    }

    public void setmLoginName(String mLoginName) {
        this.mLoginName = mLoginName;
    }

    public String getmPassWord() {
        return mPassWord;
    }

    public void setmPassWord(String mPassWord) {
        this.mPassWord = mPassWord;
    }
}
