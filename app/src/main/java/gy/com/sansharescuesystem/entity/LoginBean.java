package gy.com.sansharescuesystem.entity;

/**
 * Created by zsf 2017-07-16.
 */
public class LoginBean {
    private String flag;
    private String msg;
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMsg() {

        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFlag() {

        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
