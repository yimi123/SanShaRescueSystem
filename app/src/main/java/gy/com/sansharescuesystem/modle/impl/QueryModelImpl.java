package gy.com.sansharescuesystem.modle.impl;

import android.util.Log;

import java.io.IOException;
import java.util.Map;

import gy.com.sansharescuesystem.callback.AcyncCallBack;
import gy.com.sansharescuesystem.entity.HistoryOrbit;
import gy.com.sansharescuesystem.entity.RescueListResult;
import gy.com.sansharescuesystem.entity.ShipBean;
import gy.com.sansharescuesystem.modle.QueryModel;
import gy.com.sansharescuesystem.utils.GsonUtils;
import gy.com.sansharescuesystem.utils.HttpUtil;
import gy.com.sansharescuesystem.utils.UIUtils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by 陈秋霖 on 2017-07-14.
 */

public class QueryModelImpl implements QueryModel {
    public static String url = "http://dev.user.kfchain.com/api/alarm_info.php";
    public static String urlsn = "http://dev.user.kfchain.com/api/trail.php";
    public static String urlship = "http://dev.user.kfchain.com/api/trail.php";
    public static String historyListUrl = "http://dev.user.kfchain.com/api/alarm_info.php";
    public static String shipDataUrl = "http://dev.user.kfchain.com/api/app_index.php";

    public QueryModelImpl(){
    }
    @Override
    public void queryRescueInfo(final AcyncCallBack callBack,int page) {
        FormBody.Builder params = new FormBody.Builder();
        params.add("act","show");
        params.add("status","8");
        params.add("page",page+"");
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).post(params.build()).build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callBack.onFailed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String res = response.body().string();
                RescueListResult resp = GsonUtils.json2Bean(res,RescueListResult.class);
                callBack.onSuccess(resp);
            }
        });
    }

    @Override
    public void queryRescueInfoBySn(final AcyncCallBack callBack, String sn) {
        FormBody.Builder params = new FormBody.Builder();
        params.add("act","locus");
        params.add("sn",sn);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(urlsn).post(params.build()).build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callBack.onFailed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String res = response.body().string();
                HistoryOrbit resp = GsonUtils.json2Bean(res,HistoryOrbit.class);
                callBack.onSuccess(resp);
            }
        });
    }

    @Override
    public void queryRescueInfoByShip(final AcyncCallBack callBack, String ship) {
        FormBody.Builder params = new FormBody.Builder();
        params.add("act","locus");
        params.add("sn", ship);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(urlship).post(params.build()).build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callBack.onFailed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String res = response.body().string();
                HistoryOrbit resp = GsonUtils.json2Bean(res,HistoryOrbit.class);
                callBack.onSuccess(resp);
            }
        });
    }

    @Override
    public void queryHistoryList(final AcyncCallBack callBack,String page) {
        FormBody.Builder params = new FormBody.Builder();
        params.add("act","show");
        params.add("page",page);
        params.add("status","7");
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(historyListUrl).post(params.build()).build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callBack.onFailed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String res = response.body().string();
                Log.i("queryHistoryList",res);
                RescueListResult resp = GsonUtils.json2Bean(res,RescueListResult.class);
                callBack.onSuccess(resp);
            }
        });

    }

    @Override
    public void searchHistory(final AcyncCallBack callBack, Map<String,String> map) {
        FormBody.Builder params=new FormBody.Builder();
        params.add("act","show");
        params.add("iship",map.get("iship"));
        params.add("a_type",map.get("a_type"));
        params.add("status",map.get("status"));
        params.add("page",map.get("page"));
//        String params= historyListUrl + "?act=show"+"&iship="+map.get("iship")+"&a_type="+map.get("a_type")
//                +"&status="+map.get("status")+"&page="+map.get("page");
//        Request request = new Request.Builder().url(params).get().build();
        OkHttpClient client = new OkHttpClient();
        final Request request=new Request.Builder().url(historyListUrl).post(params.build()).build();
        Call call=client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callBack.onFailed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result=response.body().string();
                Log.i("search",result);
                RescueListResult resp = GsonUtils.json2Bean(result,RescueListResult.class);
                callBack.onSuccess(resp);
            }
        });

    }

    @Override
    public void queryShipData(final AcyncCallBack callBack, String sn) {
        OkHttpClient client = new OkHttpClient();
        final Request request =new Request.Builder().get().url(shipDataUrl+"?sn="+sn).build();
        Log.i("info","url = "+shipDataUrl+"?sn="+sn);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callBack.onFailed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String res = response.body().string();
                ShipBean bean = GsonUtils.json2Bean(res,ShipBean.class);
                Log.i("info","resp = "+bean.toString());
                callBack.onSuccess(bean);
            }
        });
    }
}
