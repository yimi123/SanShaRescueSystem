package gy.com.sansharescuesystem.modle.impl;

import android.util.Log;

import java.io.IOException;
import java.util.Map;

import gy.com.sansharescuesystem.callback.LoginCallBack;
import gy.com.sansharescuesystem.entity.LoginBean;
import gy.com.sansharescuesystem.modle.LoginModel;
import gy.com.sansharescuesystem.utils.GsonUtils;
import gy.com.sansharescuesystem.utils.HttpUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by zsf 2017-07-18.
 * function:获取登录返回数据，将数据返回给调用方
 */
public class LoginModelImpl implements LoginModel {
    @Override
    public void getData(final LoginCallBack loginCallBack, Map<String,String> map) {
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder().add("act", "login")
                .add("loginName",map.get("loginName"))
                .add("password", map.get("password"))
                .build();
        Request request = new Request.Builder().url(HttpUtil.url + "login.php").post(requestBody).build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i("LoginMode,Exception:",e.toString());
                loginCallBack.onFailed();
            }


            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();
                Log.i("info",result);
                LoginBean loginBean = GsonUtils.json2Bean(result, LoginBean.class);
                loginCallBack.onSuccse(loginBean);


            }
        });


    }
}
