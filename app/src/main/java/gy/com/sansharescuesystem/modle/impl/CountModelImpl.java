package gy.com.sansharescuesystem.modle.impl;

import android.util.Log;

import java.io.IOException;

import gy.com.sansharescuesystem.callback.CountCallBack;
import gy.com.sansharescuesystem.entity.CountBean;
import gy.com.sansharescuesystem.entity.HeatTotalBean;
import gy.com.sansharescuesystem.modle.CountModel;
import gy.com.sansharescuesystem.utils.GsonUtils;
import gy.com.sansharescuesystem.utils.HttpUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by zsf 2017-07-18.
 * function:后台获取数据，将数据返回给调用方
 */
public class CountModelImpl implements CountModel {

    /**
     *
     * @param callBack
     * function:数据统计页面获取数据
     */
    @Override
    public void getData(final CountCallBack callBack) {
        OkHttpClient okHttpClient = new OkHttpClient();
        String params=HttpUtil.url + "reports.php";
        Request request = new Request.Builder().url(params).get().build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i("CountModel,getData:","IOException:"+e.toString());
                callBack.onFailed();
            }


            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();
                Log.i("CountModelImpl:",result);
                CountBean countBean =GsonUtils.json2Bean(result,CountBean.class);
                callBack.onSuccess(countBean);


            }
        });


    }

    /**
     *
     * @param callBack
     * function:获取热力图数据
     */
    @Override
    public void getHeatData(final CountCallBack callBack) {

        OkHttpClient okHttpClient = new OkHttpClient();
        String params=HttpUtil.url + "alarm_info.php?act=show&map=reli";
        Request request = new Request.Builder().url(params).get().build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i("CountModel:","getQueryData()--IOException:"+e.toString());
                callBack.onFailed();
            }


            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();
                Log.i("info",result);
                HeatTotalBean data = GsonUtils.json2Bean(result,HeatTotalBean.class);
                callBack.onSuccess(data);


            }
        });



    }
}
