package gy.com.sansharescuesystem.modle.impl;

import java.io.IOException;

import gy.com.sansharescuesystem.callback.AcyncCallBack;
import gy.com.sansharescuesystem.entity.RescueListResult;
import gy.com.sansharescuesystem.modle.HistoryModel;
import gy.com.sansharescuesystem.utils.GsonUtils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by 陈秋霖 on 2017-07-22.
 */

public class HistoruModelImpl implements HistoryModel {
    public static final String url = "http://dev.user.kfchain.com/api/trail.php";
    @Override
    public void getHistoryData(final AcyncCallBack callBack) {
        FormBody.Builder params = new FormBody.Builder();
        params.add("act","locus");
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).post(params.build()).build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callBack.onFailed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String res = response.body().string();
                RescueListResult resp = GsonUtils.json2Bean(res,RescueListResult.class);
                callBack.onSuccess(resp);
            }
        });
    }
}
