package gy.com.sansharescuesystem.modle;


import java.util.Map;

import gy.com.sansharescuesystem.callback.AcyncCallBack;

public interface QueryModel {
    void queryRescueInfo(AcyncCallBack callBack,int page);
    void queryRescueInfoBySn(AcyncCallBack callBack,String sn);
    void queryRescueInfoByShip(AcyncCallBack callBack,String ship);
    void queryHistoryList(AcyncCallBack callBack,String page);
    void queryShipData(AcyncCallBack callBack,String sn);
    void searchHistory(AcyncCallBack callBack, Map<String,String> map);
}
