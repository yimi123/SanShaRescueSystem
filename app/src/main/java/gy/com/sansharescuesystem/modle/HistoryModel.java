package gy.com.sansharescuesystem.modle;

import gy.com.sansharescuesystem.callback.AcyncCallBack;

/**
 * Created by 陈秋霖 on 2017-07-22.
 */

public interface HistoryModel {
    void getHistoryData(AcyncCallBack callBack);
}
