package gy.com.sansharescuesystem.modle;

import java.util.Map;

import gy.com.sansharescuesystem.callback.CountCallBack;
import gy.com.sansharescuesystem.callback.LoginCallBack;

/**
 * Created by zsf on 2017-07-18.
 * function:定义登录获取后台数据接口
 */
public interface CountModel {
    void getData(CountCallBack callBack);
    void getHeatData(CountCallBack callBack);
}
